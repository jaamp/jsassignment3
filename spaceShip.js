
function SpaceShip(shipName, shipTopSpeed){

    const name = shipName;
    let topSpeed = shipTopSpeed;
    this.accelerate = function() {
        console.log(`The ${name} moving to ${topSpeed}`);
    };
    this.changeTopSpeed = function(newSpeed) {
        topSpeed = newSpeed;
    };
}

let NCC1701 = new SpaceShip('Enterprise', 100);
let ECV197 = new SpaceShip('Orville', 200);

NCC1701.accelerate();
ECV197.accelerate();

NCC1701.changeTopSpeed(150);
ECV197.changeTopSpeed(225);

NCC1701.accelerate();
ECV197.accelerate();

console.log(NCC1701.topSpeed);
console.log(ECV197.name);