const bud = {descr: 'Bud Light', price: 3.99}; 
const burger = {descr: 'Hamburger', price: 6.99};
const side = {descr: 'French Fries', price: 3.60};



const tallyBill = function(){
    let subTotal = 0;
    let item = Array.from(arguments);
    const tax = item[0];
    item.shift();
    item.forEach(function(item) {
        console.log(item.descr + '  $' + parseFloat(item.price).toFixed(2));
        subTotal += item.price;
    });
    console.log('Subtotal: $' + subTotal);
    let calcTax = (tax*subTotal).toFixed(2);
    console.log('     Tax:  $' + calcTax);
    let totalBill = parseFloat(calcTax) + parseFloat(subTotal);
    console.log('   Total: $' + totalBill);

}
tallyBill(0.1, bud, burger, side);