const team1 = { name: 'Sounders', results: 'wddwww', score: 0 };
const team2 = { name: 'Timbers', results: 'lwddll', score: 0 };
const team3 = { name: 'Galaxy', results: 'ldwlwd', score: 0 }
const team4 = { name: 'Dynamo', results: 'ddldww', score: 0 }


let standings = (function () {
    let calcResults = function () {
        let team = Array.from(arguments);
        team.forEach(function (team) {
            team.results = team.results.split('');
            for (let i = 0; i < team.results.length; i++) {
                if (team.results[i] == 'w') {
                    team.score += 3;
                }
                else if (team.results[i] == 'd') {
                    team.score += 1;
                }
                else team.score += 0;
            }
        });
        let tempTeam;
        for (let j = 0; j < team.length; j++) {
            for (let i = 0; i < team.length - 1; i++) {
                if (team[i].score < team[i + 1].score) {
                    tempTeam = team[i];
                    team[i] = team[i + 1];
                    team[i + 1] = tempTeam;
                }
            }
        }
        console.log('Standings')
        for (let i = 0; i < team.length; i++) {
            console.log((i + 1) + '  ' + team[i].name + ': ' + team[i].score);
        }
    }
    calcResults(team1, team2, team3, team4);
})();
