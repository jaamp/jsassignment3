let phoneExchange = '(206) 333-5555';
let phoneArray;
const regOne = /^(\()(\d{3})(\))(\s)(\d{3})(\-)(\d{4})$/;
//using the pipe for the 'or' allows for a space or dash or combination, 
//doesn't matter because all I want is the area code and exchange
//so I am reducing the memory of not have a third reg var or test
const regTwo = /^(\d{3})(\-|\s)(\d{3})(\-|\s)(\d{4})$/;


if (phoneExchange.match(regOne)) {
    phoneArray = phone(phoneExchange, regOne);
    console.log(phoneArray.areaCode);
    console.log(phoneArray.phoneNumber);   
}
else if (phoneExchange.match(regTwo)) {
    phoneArray = phone(phoneExchange, regTwo);
    console.log(phoneArray.areaCode);
    console.log(phoneArray.phoneNumber); 
} 
else console.log("The number is not in an approved format");

//I know function expression is preferred, but I like this way
function phone(phoneExchange, reg){
    let numArray = reg.exec(phoneExchange);
    if (numArray.length === 8) {
        return phoneArray = {
            areaCode: numArray[2],
            phoneNumber: numArray[5] + numArray[7]
        }
    }
    else {
        return phoneArray = {
            areaCode: numArray[1],
            phoneNumber: numArray[3] + numArray[5]
         }
    }
}