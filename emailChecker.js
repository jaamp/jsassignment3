let emailString = 'j.smith@gmail.com';
const emailNoDot = /^(\w+)(\@)(\w+)(\.)(\w{2,4})$/;
const emailWithDot = /^(\w+)(\.)(\w+)(\@)(\w+)(\.)(\w{2,4})$/;

if (emailString.match(emailNoDot)) {
    console.log('Email matches a no dot email');   
}
else if (emailString.match(emailWithDot)) {
    console.log('Email matches a dot email'); 
} 
else console.log('The email is not in an approved format');